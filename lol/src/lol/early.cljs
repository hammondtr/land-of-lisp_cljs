;; following along with land-of-lisp implementing simple programs/functions
;; test function evaluated in figwheel repl!
(ns lol.early)
(defn add-test [a b]
  (+ a b))

;; guess-my-number game
;; consumes

(defonce ast (atom {
  :small "1"
  :big "100"}))

(defn guess-my-number [a]
  (bit-shift-right (+ (:small @a) (:big @a)) 1))
;; alias, sure there's a better way
(defn gmn [] (guess-my-number ast))

(defn bigger []
  "swap shit")

(defn smaller []
  "smaller")


;; eventually I will draw stuff to the canvas et cetera
;; hook up with phaser and go through 4 or 5 Ludum Dare themes
